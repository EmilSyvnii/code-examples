import re
from datetime import datetime

import aiohttp
from bs4 import BeautifulSoup

from base_parser import BaseParser

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36'


class SaksOff5thParser(BaseParser):
    headers = {
        'User-Agent': USER_AGENT,
        'Accept-Language': 'en-US,en;q=0.9',
        'Proxy-Connection': 'keep-alive',
    }
    base_url = 'https://www.saksoff5th.com/'
    RE_DISCOUNT = r'(?P<discount>\d.)\%'

    async def execute_scraping(self, soup):
        result = []
        products = self.get_products_objects(soup)
        for item in products:
            url = self.get_product_url(item)
            brand = item.select_one('.product-brand')
            model = item.select_one('.pdp-link .link')
            prices = self.get_prices(item)
            msrp = prices.get('comparable_value') or prices.get('price_current')
            current_price = prices.get('sale_price') or prices.get('price_current')
            product_data = {
                'product_id': self.get_product_id(item),
                'brand': self.format_string_attrs(brand),
                'model': model.text.strip().encode('ascii', 'ignore').decode() if model else 'N/A',
                'msrp': self.get_clear_price(msrp),
                'current_price': self.get_clear_price(current_price),
                'discount': prices['total_savings'],
                'product_url': self.check_url(url),
                'alternative_product_url': '',
            }
            result.append(product_data)
        return result

    def get_prices(self, item):
        prices = {}
        comparable_value = self.get_clear_price(item.select_one('.strike-through .formatted_price')['content'])
        price_current = self.get_clear_price(item.select_one('.sales .formatted_sale_price').text.strip())
        sale_price = self.get_sale_price(item, price_current)
        prices.update({
            'comparable_value': comparable_value,
            'price_current': price_current,
            'sale_price': sale_price,              # usually comes with a promocode
            'total_savings': self.count_total_savings(
                comparable_value=comparable_value,
                price_current=price_current,
                sale_price=sale_price,
                already_clean_price=True
            ),
        })
        return prices

    def get_sale_price(self, item, price_current):
        label = item.select_one('.promotion-txt-label')
        if not label:
            return None

        promo_message = label.text.strip()
        discount = re.search(self.RE_DISCOUNT, promo_message)
        if not discount:
            return None

        sale_price = price_current - (price_current * int(discount.group('discount')) / 100)
        return round(sale_price, 2)

    @staticmethod
    def format_string_attrs(attr):
        if not attr:
            return None

        return attr.text.strip().encode('ascii', 'ignore').decode()

    @staticmethod
    def get_pagination_url(soup):
        url = soup.select_one('.show-more button')
        if url:
            return url['data-url']

        url = soup.select_one('.page-item.d-flex.next a')
        if url:
            return url['href']

        return None

    async def run_recursively(self, async_session, source_url, all_products):
        response = await self.fetch_soup(async_session, source_url)
        if not response:
            return []

        soup = BeautifulSoup(response, 'lxml')
        products = await self.execute_scraping(soup)
        if not products:
            return all_products

        all_products.extend(products)
        pagination_url = self.get_pagination_url(soup)
        if pagination_url:
            return await self.run_recursively(async_session, self.check_url(pagination_url), all_products)

        return all_products

    async def fetch_soup(self, session, source_url):
        async with session.get(
            url=source_url,
            proxy=self.get_random_proxy(),
            proxy_auth=self.proxy_auth,
            headers=self.headers,
        ) as response:
            return await response.text()

    async def gather_all_products(self, source_item, _, **kwargs):
        all_products = []
        timeout = aiohttp.ClientTimeout(total=20)
        async with aiohttp.ClientSession(timeout=timeout) as async_session:
            before_request = datetime.utcnow()                                                              # temporary
            all_products = await self.run_recursively(async_session, source_item.url, all_products)
            after_request = datetime.utcnow()                                                               # temporary
            print(f'Requesting SaksOff5th, Total duration {after_request - before_request}')
            return all_products, len(all_products)

    def get_current_products(self, items):
        products = [self.get_product_id(item) for item in items]
        middle = int(len(products) / 2)
        return products[middle:], products[:middle]

    @staticmethod
    def get_products_objects(soup):
        return soup.select('div.bfx-disable-product')

    @staticmethod
    def get_product_id(item):
        return item['data-pid']

    @staticmethod
    def get_product_url(item):
        return item.select_one('.thumb-link')['href']
