# Description

Here are examples of the code I have written recently. 

- `saks_off_5th.py` is an example of how a scraper can look like. 
- `images_processing_utils.py` and `dropbox_api.py` contain some functionality to download images and upload them to Dropbox storage.
- `dataproc_jobs_example.py` is an example of GCP Dataproc job for images validation
