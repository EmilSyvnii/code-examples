import sys
import os

import dropbox
from dropbox.exceptions import ApiError, AuthError
from dropbox.files import WriteMode

# Access token
TOKEN = os.getenv('DROPBOX_TOKEN')


def upload_image(dbx, local_image_path, folder_name):
    file_name = local_image_path.split('/')[-1]
    dbx_path = f'/{folder_name}/{file_name}'
    with open(local_image_path, 'rb') as f:
        print(f'Uploading {file_name} to Dropbox to {folder_name}...')
        try:
            dbx.files_upload(f.read(), dbx_path, mode=WriteMode('overwrite'))
        except ApiError as err:
            # Check for the specific error where a user doesn't have enough Dropbox space quota to upload this file
            if (err.error.is_path() and
                    err.error.get_path().error.is_insufficient_space()):
                sys.exit('ERROR: Cannot back up; insufficient space.')
            elif err.user_message_text:
                print(err.user_message_text)
            else:
                print(err)
            sys.exit()


def check_folder(dbx, folder_name):
    for entry in dbx.files_list_folder('').entries:
        if entry.name == folder_name:
            return True

    return False


def get_dropbox_client():
    dbx = dropbox.Dropbox(TOKEN)
    # Validate the access token
    try:
        dbx.users_get_current_account()
    except AuthError:
        print('ERROR: Invalid access token; try to re-generate an access token from the app console on the web.')
    return dbx
