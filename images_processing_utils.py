import os
import io
import requests

from PIL import Image

from dropbox_api import(
    upload_image,
    check_folder,
    get_dropbox_client
)
from utils import get_random_proxy_requests


USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'

PRODUCT_HEADERS = {
    'user-agent': USER_AGENT,
    'accept-language': 'en-US,en;q=0.9',
}

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))


def download_image(image_url):
    filename = image_url.split('/')[-1]
    file_path = os.path.join(THIS_FOLDER, filename)
    try:
        response = requests.get(
            url=image_url,
            headers=PRODUCT_HEADERS,
            proxies=get_random_proxy_requests(),
            timeout=5,
        )
        if response.status_code == 200:
            image_data = response.content
            with open(file_path, 'wb') as handler:
                handler.write(image_data)
        else:
            print('Image is unavailable')
            return None

    except Exception as e:
        print('Got an error while downloading an image:', e)
        return None

    return file_path


def image_to_bytes(local_image_path):
    """ Convert image to bytes array for storing in DB """
    an_image = Image.open(local_image_path)
    output = io.BytesIO()
    an_image.save(output, format='jpeg')
    image_as_string = output.getvalue()
    return image_as_string


def get_thumbnail(item):
    if not item['thumbnail_link']:
        return None

    local_image_path = download_image(item['thumbnail_link'])
    if not local_image_path:
        return None

    image_as_string = image_to_bytes(local_image_path)

    # remove local image
    os.remove(local_image_path)
    return image_as_string


def process_images(product):
    image_list = product['images']
    sku = str(product['sku'])
    empty_image_list = False
    if not image_list:
        empty_image_list = True
    dropbox_client = get_dropbox_client()
    if check_folder(dropbox_client, sku):
        return None

    dropbox_client.files_create_folder_v2(f'/{sku}')
    if empty_image_list:
        return None

    for image in image_list:
        local_image_path = download_image(image)
        if not local_image_path:
            continue

        upload_image(dropbox_client, local_image_path, sku)
        os.remove(local_image_path)
