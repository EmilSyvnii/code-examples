class ValidateImagesJobContext(JobContext):
    COUNTERS = (
        'total_images',
        'valid_images',
        'products_to_refresh',
    )

    def __init__(self, args):
        super().__init__()
        self.args = args

    @staticmethod
    def get_logger():
        return ExtraAggregatorLoggingAdapter(get_logger(), {'source': 'image-validator'})


class Job(BaseJob):
    VALID_IMAGE_THRESHOLD = 100  # in %

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sc = SparkContext(environment=self.env)
        self.sql_context = SQLContext(self.sc)
        self.job_context = ValidateImagesJobContext(self.kwargs)
        self.logger = self.job_context.get_logger()
        self.stores_names = self.service_config.get(self.job_name)
        self.logger.info('Store list for image validation is: %s', self.stores_names.keys())
        for counter_name in self.job_context.COUNTERS:
            self.job_context.initalize_counter(self.sc, counter_name)

    def run(self):
        self.logger.info('Starting the process of image validation')
        loop = asyncio.get_event_loop()
        stores, stores_id_list = loop.run_until_complete(self.get_stores_id_by_name(self.stores_names.keys()))
        if not stores_id_list:
            self.logger.error('No stores to validate images')
            raise Exception

        query = get_export_query(stores_id_list)
        self.logger.info('Query for scheduling is: %s', query)
        get_es_products_rdd(self.sc, query) \
            .repartition(5) \
            .mapPartitions(partial(self.run_by_partition, context=self.job_context, stores=stores)) \
            .count()
        self.job_context.print_counters()
        self.logger.info('Finished images validation')

    @staticmethod
    async def get_stores_id_by_name(stores_names):
        stores_id_list = []
        stores = await get_visible_scraped_stores()
        for store in stores:
            if store.classifiedStoreName in stores_names:
                stores_id_list.append(store.entityId)
        return stores, stores_id_list

    @staticmethod
    def run_by_partition(items, context, stores):
        loop = asyncio.get_event_loop()
        logger = ExtraAggregatorLoggingAdapter(get_logger(), {'source': 'image-validator'})
        loop.run_until_complete(Job.exec_async_validation(items, context, stores, logger))
        return []

    @staticmethod
    async def exec_async_validation(items, context, stores, logger):
        publisher = ProductScrapingPublisher(
            scrape_type='full',
            stores=stores,
            logger=logger,
        )
        products_to_publish = []
        for chunk in chunker(items, chunk_size=50):
            tasks = [Job.validate_product(product, context, logger) for product in chunk]
            result = await asyncio.gather(*tasks)
            for item in result:
                if item:
                    products_to_publish.append(item)
        if products_to_publish:
            await publisher.publish(products_to_publish, source='image-validator')
        return products_to_publish

    @staticmethod
    async def validate_product(product, context, logger):
        product_id, product_data = product
        images = [url['url'] for url in product_data['slice_info_list'][0]['images']]
        request_results = await Job.make_head_requests(images)
        total_images = len(images)
        valid_images = 0
        for url, code in request_results:
            status_code = code.status
            logger.info('Result status %s for image %s', status_code, url)
            if status_code in (200, 301):
                valid_images += 1

        context.inc_counter('total_images', value=total_images)
        context.inc_counter('valid_images', value=valid_images)
        if valid_images / total_images * 100 < Job.VALID_IMAGE_THRESHOLD:
            logger.info('Publish product %s to full after image validation', product_id)
            context.inc_counter('products_to_refresh')
            return product

        return None

    @staticmethod
    async def make_head_requests(urls):
        async with aiohttp.ClientSession() as session:
            tasks = [session.head(url) for url in urls]
            results = await asyncio.gather(*tasks)
            return zip(urls, results)
